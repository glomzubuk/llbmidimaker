﻿using System;
using System.IO;
using UnityEngine;
using GameplayEntities;
using LLHandlers;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;
using LLBML;
using LLBML.Players;
using LLBML.Math;
using LLBML.States;
using MidiSharp;
using MidiSharp.Events;
using MidiSharp.Events.Meta;
using MidiSharp.Events.Voice.Note;

namespace LLBMidiMaker
{
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInProcess("LLBlaze.exe")]
    public class LLBMidiMaker : BaseUnityPlugin
    {
        public static LLBMidiMaker Instance { get; private set; } = null;
        public static ManualLogSource Log { get; private set; } = null;
        internal static DirectoryInfo ModdingFolder { get; private set; }

        private bool initialized_midi;
        private FileInfo midiFile;
        private float startTime;
        private long DeltaTime => (long)((Time.time - startTime) / 60);
        MidiTrack track = null;


        private void Awake()
        {
            Instance = this;
            Log = this.Logger;
            ModdingFolder = LLBML.Utils.ModdingFolder.GetModSubFolder(this.Info);
            Logger.LogInfo("Hello, World!");

            var harmoInstance = new Harmony(PluginInfos.PLUGIN_ID);
        }

        private void FixedUpdate()
        {
            if (GameStates.IsInMatch())
            {
                if (!initialized_midi)
                {
                    InitMidiSequence();
                    initialized_midi = true;
                }

                RegisterFrameMidi();
            }
            else
            {
                if (initialized_midi)
                {
                    initialized_midi = false;
                    EndMidiSequence();
                }
            }
        }

        private byte scaleToByte (int toScale, int lowRange, int highRange)
        {
            toScale -= lowRange;
            highRange -= lowRange;
            return (byte)((toScale * 127) / highRange);
        }

        private void InitMidiSequence()
        {
            midiFile = new FileInfo(Path.Combine(ModdingFolder.FullName, $"{DateTime.Now.ToUniversalTime().ToString("s").Replace(':','-')}.mid"));

            track = new MidiTrack();
            startTime = Time.time;
        }

        private void RegisterFrameMidi()
        {
            if (BallApi.BallsActivelyInMatch() && BallApi.GetBall() is BallEntity ball)
            {
                Vector2f stageMin = World.instance.stageMin;
                Vector2f stageMax = World.instance.stageMax;

                Vector2f currentPos = ball.entityData.position;
                PlayerEntity pe = ball.GetLastPlayerHitter();
                byte channel = (byte)(pe != null ? (Team)pe.GetTeam() : Team.NONE);
                byte note = scaleToByte((int)currentPos.x, (int)stageMin.x, (int)stageMax.x);
                byte velocity = scaleToByte((int)currentPos.y, (int)stageMax.y, (int)stageMin.y);
                MidiEvent[] midiEvents = NoteVoiceMidiEvent.Complete(DeltaTime, channel, note, velocity, 1);
                foreach (MidiEvent ev in midiEvents)
                {
                    track.Events.Add(ev);
                }
            }
        }

        private void EndMidiSequence()
        {
            track.Events.Add(new EndOfTrackMetaMidiEvent(DeltaTime));
            MidiSequence sequence = new MidiSequence(Format.One, 60);
            sequence.Tracks.Add(track);
            Logger.LogDebug($"sequence tracks: {sequence.ToString()}");
            using (Stream outputStream = midiFile.OpenWrite())
            {
                sequence.Save(outputStream);
            }
            sequence = null;
            track = null;
            midiFile = null;
            startTime = -1;
        }
    }
}
